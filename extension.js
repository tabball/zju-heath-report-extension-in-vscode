// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require('vscode');
const request = require('request');
var Login = require('./login.js');
var DK = require("./DK.js");

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed

/**
 * @param {vscode.ExtensionContext} context
 */
function activate(context) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "zjuhealthreport" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with  registerCommand
	// The commandId parameter must match the command field in package.json
	// let disposable = vscode.commands.registerCommand('zjuhealthreport.helloWorld', function () {
	// 	// The code you place here will be executed every time your command is executed

	// 	// Display a message box to the user
	// 	vscode.window.showInformationMessage('Hello World from ZJUHealthReport!');
	// });

	// context.subscriptions.push(disposable);

	context.subscriptions.push(vscode.commands.registerCommand('zjuhealthreport.report', function () {
		vscode.window.showInputBox({
			ignoreFocusOut: true,
			title: '用户名',
		}).then((username) => {
			if (username == undefined)
				return;
			vscode.window.showInputBox({
				title: '密码',
				password: true,
				ignoreFocusOut: true
			}).then((password) => {
				if (password == undefined)
					return;

				// login
				// vscode.window.withProgress({
				// 	cancellable: true,
				// 	title: "正在进行每日健康打卡",
				// 	location: vscode.ProgressLocation.Notification,
				// }, (progress, token) => {
				// 	token.onCancellationRequested(() => {
				// 		vscode.window.showInformationMessage("填报取消");
				// 	});
				// return new Promise((resolve) => {
				// progress.report({ increment: 30, message: "登录至统一认证" });
				vscode.window.showInformationMessage("正在登录至统一认证")
				Login.login(username, password).then((value) => {
					var response = value.response;
					var jar = value.jar;
					var headers = value.headers;
					// DK.DK(response, jar, headers, progress).then(() => {
					// 	resolve();
					// });
					DK.DK(response, jar, headers)
				});
				// })

			});
		});
	}));
		
}

// this method is called when your extension is deactivated
function deactivate() { }

module.exports = {
	activate,
	deactivate
}
